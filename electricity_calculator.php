<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device=width, initial-scale=1">
        <title>Kiraan TNB</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://getbootstrap.com/docs/5.2/assets/css/docs.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <style>
        html{
            text-align: center;
            margin: 50px;
        }
        h1{
            text-align: center;
        }
    
    </style>
    <body>
        <h1>Calculate</h1>
        <br>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
            <div class="form-group">
                <label for="InputVoltage">Voltage</label><br>
                <input type="number" step="0.01" class="form-control" id="voltage" name="voltage" required>
                <small id="numberVoltage" class="form-text text-muted">Voltage (V)</small>
                <br><br>
            </div>
            <div class="form-group">
                <label for="InputCurrent">Current</label><br>
                <input type="number" step="0.01" class="form-control" id="current" name="current" required>
                <small id="numberCurrent" class="form-text text-muted">Ampere (A)</small>
                <br><br>
            </div>
            <div class="form-group">
                <label for="InputCurrentRate">CURRENT RATE</label><br>
                <input type="number" step="0.01" class="form-control" id="rate" name="rate" required>
                <small id="numberRate" class="form-text text-muted">sen/kWh</small>
                <br><br>
            </div>
            <button type="submit" class="btn btn-primary">Calculate</button>
        </form>
        <br>
        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $voltage = $_POST['voltage'];
            $current = $_POST['current'];
            $rate = $_POST['rate'];

            $power = $voltage * $current;
            $energy = $power * 24 * 1000;
            $energy * ($rate / 100);

            echo "<h2>Calculation Results:</h2>";
            echo "<p>Power: " . $power/1000 ."kW</p>";
            echo "<p>Rate: " . $rate/100 ."RM</p>";
}
?>
    </body>
</html>


